# surface

Command surface provides rudimental checking of API changes/differences.

# Installation

     $ go install modernc.org/surface@latest

# Usage

To produce a list of exported symbols for a package in the current
directory issue

     $ surface

or  perhaps

     $ surface > foo

To check for removed symbols from two lists issue

     $ surface old new

This reports symbols listed in old but not in new.

# Options

     -tags <csv-list>

This option has the same meaning as in the go tool. Use it to add build tags
that may change which files will be considered a part of the package.

# Notes

The tool works on parser ASTs and considers only exported names. No
typechecking is performed, it does not understand nor care about changes in
definitions.

The package loader respects environment variables GOOS and GOARCH. Files
matching `*_test.go` are ignored.
