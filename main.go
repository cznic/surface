// Copyright 2024 The Surface Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Command surface provides rudimental checking of API changes/differences.
//
// # Installation
//
//	$ go install modernc.org/surface@latest
//
// # Usage
//
// To produce a list of exported symbols for a package in the current
// directory issue
//
//	$ surface
//
// or  perhaps
//
//	$ surface > foo
//
// To check for removed symbols from two lists issue
//
//	$ surface old new
//
// This reports symbols listed in old but not in new.
//
// # Options
//
//	-tags <csv-list>
//
// This option has the same meaning as in the go tool. Use it to add build tags
// that may change which files will be considered a part of the package.
//
// # Notes
//
// The tool works on parser ASTs and considers only exported names. No
// typechecking is performed, it does not understand nor care about changes in
// definitions.
//
// The package loader respects environment variables GOOS and GOARCH. Files
// matching *_test.go are ignored.
package main

import (
	"flag"
	"fmt"
	"go/token"
	"os"
	"path/filepath"
	"slices"
	"strings"

	"modernc.org/fileutil/ccgo"
	"modernc.org/gc/v3"
)

func fail(rc int, s string, args ...any) {
	s = fmt.Sprintf(s, args...)
	if !strings.HasSuffix(s, "\n") {
		s += "\n"
	}
	fmt.Fprint(os.Stderr, s)
	os.Exit(rc)
}

// BUG(jnml)
//
// The package loader in use handles only a subset of the targets supported by
// Go.  Please fill an issue if you need to add another target. Additionally,
// the parser does not really support generics above few simple cases.
func main() {
	cwd, err := ccgo.AbsCwd()
	if err != nil {
		fail(1, "%s", err)
	}

	oTags := flag.String("tags", "", "comma-separated tag list")
	flag.Parse()
	tags := strings.Split(*oTags, ",")
	switch flag.NArg() {
	case 0:
		err = list(cwd, tags)
	case 2:
		err = check(flag.Arg(0), flag.Arg(1))
	default:
		fail(2, "zero or two arguments expected")
	}
	if err != nil {
		fail(1, "%s", err)
	}
}

func check(a, b string) (err error) {
	f, err := loadAPI(a)
	if err != nil {
		return err
	}

	g, err := loadAPI(b)
	if err != nil {
		return err
	}

	for k := range g {
		delete(f, k)
	}
	var s []string
	for k := range f {
		s = append(s, k)
	}
	slices.Sort(s)
	for _, k := range s {
		fmt.Println(k)
	}
	if len(s) != 0 {
		err = fmt.Errorf("incompatible changes detected")
	}
	return err
}

func loadAPI(nm string) (m map[string]struct{}, err error) {
	b, err := os.ReadFile(nm)
	if err != nil {
		return nil, err
	}

	m = map[string]struct{}{}
	for _, k := range strings.Split(string(b), "\n") {
		m[k] = struct{}{}
	}
	return m, nil
}

func list(dir string, tags []string) (err error) {
	cfg, err := gc.NewConfig(gc.ConfigBuildTags(tags))
	if err != nil {
		return err
	}

	ip, err := filepath.Rel(filepath.Join(os.Getenv("GOPATH"), "src"), dir)
	if err != nil {
		return err
	}

	exports, err := load(cfg, ip)
	if err != nil {
		fail(1, "%s", err)
	}

	var a []string
	for k := range exports {
		a = append(a, k)
	}
	slices.Sort(a)
	for _, k := range a {
		fmt.Println(k)
	}
	return nil
}

func load(cfg *gc.Config, ip string) (exports map[string]gc.Node, err error) {
	pkg, err := cfg.NewPackage("", ip, "", nil, false, gc.TypeCheckNone)
	if err != nil {
		return nil, err
	}

	exports = map[string]gc.Node{}
	pkg.Scope.Iterate(func(name string, n gc.Node) (stop bool) {
		if token.IsExported(name) {
			exports[name] = n
		}
		return false
	})
	return exports, nil
}
