module modernc.org/surface

go 1.21.0

require (
	modernc.org/fileutil v1.3.0
	modernc.org/gc/v3 v3.0.0-20240304020402-f0dba7c97c2b
)

require (
	github.com/dustin/go-humanize v1.0.1 // indirect
	github.com/hashicorp/golang-lru/v2 v2.0.7 // indirect
	github.com/remyoudompheng/bigfft v0.0.0-20230129092748-24d4a6f8daec // indirect
	modernc.org/mathutil v1.6.0 // indirect
	modernc.org/strutil v1.2.0 // indirect
	modernc.org/token v1.1.0 // indirect
)
