# Copyright 2024 The Surface Authors. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

.PHONY:	all build_all_targets check clean edit editor test work

SHELL=/bin/bash -o pipefail	

all: editor
	golint 2>&1
	staticcheck 2>&1

clean:
	rm -f log-* cpu.test mem.test *.out
	git clean -fd
	go clean

check:
	staticcheck 2>&1 | grep -v U1000

edit:
	@touch log
	@if [ -f "Session.vim" ]; then gvim -S & else gvim -p Makefile all_test.go main.go & fi

editor:
	gofmt -l -s -w *.go 2>&1 | tee log-editor
	go test -v 2>&1 | tee -a log-editor
	go install -v  2>&1 | tee -a log-editor

test:
	echo -n > /tmp/ccgo.log
	touch log-test
	cp log-test log-test0
	go test -v -timeout 24h -count=1 2>&1 | tee log-test
	grep -a 'TRC\|TODO\|ERRORF\|FAIL' log-test || true 2>&1 | tee -a log-test


work:
	rm -f go.work*
	go work init
	go work use .
